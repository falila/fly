===
FR - Where To Sleep Tonight

Where To Sleep Tonight est une application de recherche d'hôtels qui permet de trouver des établissements autour de la position de l'usagé. 
Ce dernier a la possibilité de consulter des informations sur les hôtels telles que le numùéro de téléphone l' adresse ...
L'utilisateur peut également ajouter un établissement dans ses favoris.
L'application permet consulter la distance entre la position de l'utilisateur et un hôtel présent sur la carte. 
il permet aussi de calculer l'itineaire entre l'utilisateur et l'etablissement hotelier choisit.
Enfin, on peut ajouter des notes sur les services (confort, qualité-hygiène, wifi, nourriture etc..) un commentaire sur le lieu où on a passé une nuit.

===
EN - Where To Sleep Tonight

Where To Sleep Tonight is a search application that allows users to find hotels around their position. 
It enables to view information about hotels such as phone number, address ... 
The user can also add a hotel in his list of favorite hotles. 
The application can also show the distance between the position of the user and a hotel that appears on the map.
Finally, the user can post a comment about the hotel where he spent the night.
