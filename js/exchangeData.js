var indexedDB=window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;

//prefixes of window.IDB objects
window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange

if (!window.indexedDB) {
  window.alert("Your browser doesn't support a stable version of IndexedDB. Such and such feature will not be available.")
}

function createDatabase(event){
  var db = event.target.transaction.db;
  var favoriStore = db.createObjectStore("favorites", {keyPath:"id", autoIncrement:true});
  var hotelStore = db.createObjectStore("hotels", {keyPath:"id", autoIncrement:true});
}



/**
* Erreur ouverture de la base de donn?
*/
function errorOpen(event) {
  window.alert("Erreur ouverture !");
}

/**
*Enregistrer dans la base de donn?
*/
function saveFromMap(nom, adresse, latitude, longitude, t�l�phone){
  var hotel = {
    nom:nom,
    adresse:adresse,
    latitude:latitude,
    longitude:longitude,
    t�l�phone:t�l�phone
  }

  // on ouvre la base, et on d?are les listeners
  var request = indexedDB.open("testWtsl", 1);
  request.onerror = errorOpen;
  request.onupgradeneeded = createDatabase;

  // ici la base a ? ouverte avec succ? il faut ajouter l'enregistrement
  request.onsuccess = function(event) { 
  
    // on r?p? l'objet database
    var db = event.target.result; 

    // on ouvre une transaction qui permettra d'effectuer
    // les op?tions sur la base
    var transaction = db.transaction(["hotel"], "readwrite")
	transaction.oncomplete = function(event) {
      window.alert("Sauvegarde effectu�e");
    };
	transaction.onerror = function(event) {
      window.alert('Erreur dans la sauvegarde');
    };
	
	var hotelStore = transaction.objectStore("hotel");
	    
    // on cr?l'ordre d'ajouter un enregistrement
    // sera effectivement execut?ors de la fermeture de la transaction
    var req = hotelStore.add(hotel);
	req.onsuccess = function (event){
	}
    req.onerror = function(event) {
      alert("Erreur pour ajouter des donn?");
    };
  }; 
}
 
function saveFromFavorites(nom, prenom, adresse, latitude, longtitude, telephone, prix, wifi, animaux, voiturier, h_q, avis){

  var favorites ={
	nom: nom, 
    prenom: prenom, 
	adresse:adresse,
	latitude:latitude,
	longtitude:longtitude,
	telephone:telephone,
	prix:prix,
	wifi:wifi,
	animaux:animaux,
	voiturier:voiturier,
	h_q:h_q,
	avis:avis
  } 
  
  // on ouvre la base, et on d?are les listeners
  var request = indexedDB.open("testWtsl", 1);
  request.onerror = errorOpen;
  request.onupgradeneeded = createDatabase;

  // ici la base a ? ouverte avec succ? il faut ajouter l'enregistrement
  request.onsuccess = function(event) { 
  
    // on r?p? l'objet database
    var db = event.target.result; 

    // on ouvre une transaction qui permettra d'effectuer
    // les op?tions sur la base
    var transaction = db.transaction(["favorites"], "readwrite")
	transaction.oncomplete = function(event) {
      window.alert("Sauvegarde effectu�e");
    };
	transaction.onerror = function(event) {
      window.alert('Erreur dans la sauvegarde');
    };
	
	var favoriStore = transaction.objectStore("favorites");
	    
    // on cr?l'ordre d'ajouter un enregistrement
    // sera effectivement execut?ors de la fermeture de la transaction
    var req = favoriStore.add(favorites);
	req.onsuccess = function (event){
	}
    req.onerror = function(event) {
      alert("Erreur pour ajouter des donn?");
    };
  }; 
}

/*
function updateFromFavorites (id,nom, prenom, adresse, latitude, longtitude, telephone, prix, wifi, animaux, voiturier, h_q, avis){
	var fav = searchById(id); 
	console.log(fav); 
}
*/

/**
* Recherche d'element
*/
/*
function searchById(id){
		
  var request = indexedDB.open("testWtsl", 1);
  request.onerror = errorOpen;
  request.onupgradeneeded = createDatabase;  
  var fav;
   
  request.onsuccess = function(event) {
    var db = event.target.result;
    var transaction = db.transaction(["favorites"], "readwrite");
    var favoriteStore = transaction.objectStore("favorites");
    var req = favoriteStore.get(parseInt(id));
	req.onsuccess = function(event)	{
	  fav = {
	   nom: nom, 
       prenom: prenom, 
	   adresse:adresse,
	   latitude:latitude,
	   longtitude:longtitude,
	   telephone:telephone,
	   prix:prix,
	   wifi:wifi,
	   animaux:animaux,
	   voiturier:voiturier,
	   h_q:h_q,
	   avis:avis
	  }
	  //console.log(fav); 
	  //console.log(JSON.stringify(fav))  ;
	  return fav;
	};
	req.onerror = function(event)	{
	  console.log("erreur"); 
	};
  }
}
*/

/**
* Supprime le favori
**/
function deleteFromId(id){

  var request = indexedDB.open("testWtsl", 1);
  request.onerror = errorOpen;
  request.onupgradeneeded = createDatabase;
  
  request.onsuccess = function(event) {
    var db = event.target.result;
	
	// on ouvre une transaction qui permettra d'effectuer la suppression
    var transaction = db.transaction(["favorites"], "readwrite");
	transaction.oncomplete = function(event) {
      window.alert("Suppression effectu�e");
    };
    transaction.onerror = function(event) {
      window.alert('erreur dans la suppression');
    };
	
 	var favoriStore = transaction.objectStore("favorites");
	
	var req = favoriStore.delete( parseInt(id));
    req.onsuccess = function(event) {
    };
    req.onerror = function(event) {
      window.alert('Erreur suppression');
    };
  }
}