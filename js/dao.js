/*********File Dao favorite*******
cette solution est une alternative à l'utilisation de indexdb.
car,l'applicaiton Where sleep tonigth stocke une très faible quantité de donnees 
**/
/*************@author souleymane keita ******/
/*************version 0.1 *******************/
/*************date samedi 30 mars 2013 ******/
/*************  debut 19h32mn***************/
/************fin 20h15mn ********************/
var DAO=function(){};


/************ genereted a favorite key with current time *****/
/************ Genere un id pour le nouveau favoris a creer****/
DAO.createNewFavoriteKey=function(){

        var currentDate = new Date();
        var time = currentDate.getTime();
        var key = "fav_" + time;
        
        
       return key;

};

/********* save a favorite form google maps ***********/
/********** sauvegarde un hotel se trouvant sur la map dans DB****/
DAO.saveFavoriteFormMap=function(fa){

        var tempFav=fa;
        var favorisKeyArray = DAO.getkeyArrays();
        //tempFav.id=DAO.createNewFavoriteKey();
        
        try {
                localStorage.setItem(tempFav.id, JSON.stringify(tempFav));
                favorisKeyArray.push(tempFav.id);
                localStorage.setItem("favorisKeyArray", JSON.stringify(favorisKeyArray));
                console.log("save in BD / enregistré dans la base")
                
        } catch(e) {
               
                alert("Debordement de stockage! / erreur inconnus in saveFavoriteFormMap");
        
}

};

/******* save a favorite in localStorage **********/
/******* sauvegarde un favoris dans la db local ****/
DAO.saveFromFavorites=function(id,nom,adresse,telephone,prix,Wifi,animaux,voiturier,h_q,avis,longitude,latitude){

        var tempFav=new Favorite(id,nom,addresse,telephone,prix,Wifi,animaux,voiturier,h_q,avis,longitude,latitude);
        var favorisKeyArray = DAO.getkeyArrays();
               
        try {
                DAO.deleteFavorite(tempFav.id);
                localStorage.setItem(tempFav.id, JSON.stringify(tempFav));
                localStorage.setItem("favorisKeyArray", JSON.stringify(favorisKeyArray));

        } catch(e) {
        
      
                alert("Debordement de stockage!/ erreur inconnues dans saveFromFavorites");
        
        }

};


/********** update a favorite in DB storage **************/
/********** met à jour un favoris dans la DB *********/
DAO.updateFavorite=function(id,nom,addresse,telephone,prix,Wifi,animaux,voiturier,h_q,avis,longitude,latitude){
		  DAO.deleteFavorite(id);
		  
          var newFavorite=new Favorite();  	
		  
          newFavorite.id=DAO.createNewFavoriteKey();
		  newFavorite.nom=nom;
		  newFavorite.addresse=addresse;
		  newFavorite.telephone=telephone;
		  newFavorite.longitude=longitude;
		  newFavorite.latitude=latitude;
		  newFavorite.h_q=h_q;
		  newFavorite.voiturier=voiturier;
		  newFavorite.wifi=Wifi;
		  newFavorite.animaux=animaux;
		  newFavorite.prix=prix;
		  newFavorite.avis=avis;
			
	  
        try {
                
                localStorage.setItem(newFavorite.id, JSON.stringify(newFavorite));
				var favorisKeyArray = DAO.getkeyArrays();
				favorisKeyArray.push(newFavorite.id);
                localStorage.setItem("favorisKeyArray", JSON.stringify(favorisKeyArray));

        } catch(e) {
                alert("Debordement de stockage!/ updateFavorite");
        }

};


/********supp a favorite *******/
/******** supprime un favoris d'id key ***/
DAO.deleteFavorite=function(key){

        localStorage.removeItem(key);
        var keyArray=DAO.getkeyArrays();

        if(keyArray)
        {
                for(var i = 0; i < keyArray.length; i++)
                {
                        if(key==keyArray[i]){
                                keyArray.splice(i,1);
                        }
                        
                }
        }
        localStorage.setItem("favorisKeyArray", JSON.stringify(keyArray));

        
};

/*******get favorite key array ****************/
/******* return la liste de clé des valeur de la DB **/
DAO.getkeyArrays=function(){

        var keyArray = localStorage.getItem("favorisKeyArray");

        if (!keyArray)
         {        
                keyArray = [];
                localStorage.setItem("favorisKeyArray", JSON.stringify(keyArray));
        } else
             {
                 keyArray = JSON.parse(keyArray);
             }
        return keyArray;
        };

/*****clear all all favorite ******/
/***** supprime tous les FAV ***/
DAO.clear=function(){
var rep = confirm("Voulez-vous supprimer tous les hôtels ?");
	if (rep == true){
		localStorage.clear();
       		alert("Les Hôtels ont été supprimés!");
	}
        
};


/*********** get all favorite form local storage *******/
/************ return tous les favoris  sous forme de string ********/
/*********** ATTENTION: utiliser JSON.parse pour extraire les donnees ***/
DAO.getAllFavorite=function(){
        
        var listeFavoris=[];
        var KeyArray=DAO.getkeyArrays();
        
        if(!KeyArray)
        {
                console.log("list favoris vide/empty");
        }else
             {
                for(i = 0; i < KeyArray.length; i++)
                {
                        listeFavoris[i]=JSON.parse(localStorage.getItem(KeyArray[i]));
                        //listeFavoris[i]=localStorage.getItem(keyArray[i]);
                }
                console.log("fin/end recherche");
             }
   
   return JSON.stringify(listeFavoris);
  // return listeFavoris;
};


/****************get favorite by id **************/
/******** return un favoris dont la clé est passe en param ***/
DAO.getFavoriteById=function(key){
        var tempkey=key;
        var keyArray=DAO.getkeyArrays();
        
        if(tempkey == "")
        {
                 console.log("cle /key non valide");
        }
        
        if(keyArray)
        {
                var cmpt=0;        
                while(keyArray[cmpt]!=tempkey && cmpt < keyArray.length)
                {
                        cmpt++;
                        
                }
                if(keyArray[cmpt]==tempkey)
                {
                       return JSON.stringify(localStorage.getItem(tempkey));
                }else
                       {
                                console.log("favorite not found /object non trouvé");
                       }
        }else
             {
                console.log("la liste favoris est vide / list is empty");
             }
   };
   
   
      // class favorite 
    function  Favorite (id,nom,addresse,telephone,prix,wifi,animaux,voiturier,h_q,avis,longitude,latitude){
    	
    	id=id;
    	nom=nom;
    	addresse=addresse;
    	telephone=telephone;
    	prix=prix;
    	wifi=wifi;
    	animaux=animaux;
    	voiturier=voiturier;
    	h_q=h_q;
    	avis=avis;
    	longitude=longitude;
    	latitude=latitude; 	
    	
    }
    
    Favorite= { "id":"","nom":"","addresse":"","telephone":"","prix":"","wifi":"","animaux":"","voiturier":"","h_q":"","avis":"","longitude":"","latitude":""};
    
   
