// fonction d'affichage des sections permettant 
// d'afficher/modifier les informations d'un h�tel
function openSection(section, num){

	if (section == "#attributes"){
	//Section d'affichage des informations d'un h�tel
		populateSection('attributes', num);
		
		$(section+num).slideToggle("slow");
		$("#editing"+num).toggle(false);

		var att; var edit;
		var ids = DAO.getkeyArrays();
		for(var i=0; i<ids.length; i++){
			if(ids[i] != num){
				att = "#attributes"+ids[i];
				edit = "#editing"+ids[i];
				$(att).toggle(false);
				$(edit).toggle(false);
			}
		}
	} else {
		//Section de modification des informations d'un h�tel
		populateSection('editing', num);
		
		$(section+num).slideToggle("slow");
		$("#attributes"+num).toggle(false);

		
		var att; var edit;
		var ids = DAO.getkeyArrays();
		for(var i=0; i<ids.length; i++){
			if(ids[i] != num){
				att = "#attributes"+ids[i];
				edit = "#editing"+ids[i];
				$(att).toggle(false);
				$(edit).toggle(false);
			}
		}
	}
			
}

// Fonction d'int�gration dynamique des favoris dans la page Favoris
function uploadFavorites(){

	//Donn�es JSON
	var obj = DAO.getAllFavorite();
	var favorites = JSON.parse(obj);
	
	var cont = '';
	if(favorites == ''){
		cont += '<center><img src="img/page-blanche.jpg"></center>';

	}else{
		for(var h=0; h<favorites.length; h++){
				//Ent�te
				cont += '<section role="headline">';
				cont += '<header><menu type="toolbar"><a href="#" onclick="openSection(\'#editing\', \''+favorites[h].id+'\')"><span class="icon icon-edit">edit</span></a>';
				cont += '<a href="#" onclick="deleteFavorite(\''+favorites[h].id+'\')"><span class="icon icon-delete">delete</span></a></menu>';
				cont += '<h1 id="hotel'+favorites[h].id+'" onclick="openSection(\'#attributes\', \''+favorites[h].id+'\')">'+favorites[h].nom+'</h1></header>';
				cont += '</section>';
				
				//Section infos
				cont += '<section class="attributes" id="attributes'+favorites[h].id+'"></section>';
				
				//Section modifs
				cont += '<section class="editing" id="editing'+favorites[h].id+'"></section>';

		}
	}
	//Int�gration dans la page visualis�e
	document.getElementById("content").innerHTML = cont;

}


// fonction de peuplement des sections informations hotels et modification donn�es hotel, vides au moment du chargement de la page
function populateSection(section, num){
	var contentSection = document.getElementById(section + num).innerHTML;
	
	var newContent = '';
	
	var obj = DAO.getAllFavorite();
	var favorites = JSON.parse(obj);
	
	var newContent = '';
	
	
	for(var h=0; h<favorites.length; h++){
	
		if (favorites[h].id == num){
			//if (section == "attributes" && contentSection == "") 
			if (section == "attributes") {
				//section affichage des informations de l'hotel
				newContent += '<h2 class="bb-docs">Informations</h2>';
				newContent += '<ul><li data-state="marked"><dl><dd><span>Adresse</span></dd>';
				newContent += '<dt>'+favorites[h].addresse+'</dt></dl></li>';
				newContent += '<li><em class="aside"><span class="icon icon-callout">Outgoing call</span></em><dl><dd><span>N� T�l�phone</span></dd><dt>'+favorites[h].telephone+'</dt></dl></li>';
				
				
				//Note sur le prix
				newContent += '<li data-state="tagged" data-tag="A"><dl><dt><table><td>Prix<td>';
				newContent += '<td><div role="notation">';
				switch (favorites[h].prix) {
					 case '0':
						newContent += '<ul class="rating nostar"></ul>';break;
					 case '1':
						newContent += '<ul class="rating onestar"></ul>';break;
					 case '2':
						newContent += '<ul class="rating twostar"></ul>';break;
					 case '3':
						newContent += '<ul class="rating threestar"></ul>';break;
					 case '4':
						newContent += '<ul class="rating fourstar"></ul>';break;
					 case '5':
						newContent += '<ul class="rating fivestar"></ul>';break;
					 default: 
						newContent += '<ul class="rating nostar"></ul>';break;
				}
				newContent += '</div></td></table></dt>';
				
				
				//Note sur la qualit�
				newContent += '<dt><table><td>Hygi�ne-Qualit� <td><td><div role="notation">';
				switch (favorites[h].h_q) {
					 case '0':
						newContent += '<ul class="rating nostar"></ul>';break;
					 case '1':
						newContent += '<ul class="rating onestar"></ul>';break;
					 case '2':
						newContent += '<ul class="rating twostar"></ul>';break;
					 case '3':
						newContent += '<ul class="rating threestar"></ul>';break;
					 case '4':
						newContent += '<ul class="rating fourstar"></ul>';break;
					 case '5':
						newContent += '<ul class="rating fivestar"></ul>';break;
					 default: 
						newContent += '<ul class="rating nostar"></ul>';break;
				}
				newContent += '</div></td></table></dt></dl></li>';
				
				
				newContent += '<li data-state="tagged" data-tag="B"><dl><dt>WiFi <em>'+favorites[h].wifi+'</em></dt><dt>Animaux <em>'+favorites[h].animaux+'</em></dt><dt>Voiturier <em>'+favorites[h].voiturier+'</em></dt></dl></li>';
				newContent += '<ul><li data-state="marked"><dl><dd><span>Avis</span></dd>';
				newContent += '<dt>'+favorites[h].avis+'</dt></dl></li></ul>';
			
				document.getElementById(section + num).innerHTML = newContent;
				break;
			}else if (section == "editing"){
				//section modification des donn�es de l'hotel
				newContent += '<h2 class="bb-docs">Modification</h2>';
				newContent += '<form role="edit" name="form'+favorites[h].id+'" >';
				newContent += '<p><fieldset><legend>Nom</legend><section><p><input type="tel" name="nom" value="'+favorites[h].nom+'" required><button type="reset">Clear</button></p><p></p></section></fieldset>';
				
				//Contenu non affich�(adresse, longitude, latitude, telephone)
				newContent += '<input type="hidden" name="adresse" value="'+favorites[h].addresse+'">';
				newContent += '<input type="hidden" name="longitude" value="'+favorites[h].longitude+'">';
				newContent += '<input type="hidden" name="latitude" value="'+favorites[h].latitude+'">';
				newContent += '<input type="hidden" name="telephone" value="'+favorites[h].telephone+'">';
				
				//Notation Prix
				newContent += '<p><table><td>Prix</td><td><div role="notation"><ul id="prix'+favorites[h].id+'" class="rating nostar">';
				newContent += '<li class="one"><a href="#" title="1 Star" onclick="changeRating(\'prix\', \''+favorites[h].id+'\', 1)">1</a></li>';
				newContent += '<li class="two"><a href="#" title="2 Stars" onclick="changeRating(\'prix\', \''+favorites[h].id+'\', 2)">2</a></li>';
				newContent += '<li class="three"><a href="#" title="3 Stars" onclick="changeRating(\'prix\', \''+favorites[h].id+'\', 3)">3</a></li>';
				newContent += '<li class="four"><a href="#" title="4 Stars" onclick="changeRating(\'prix\', \''+favorites[h].id+'\', 4)">4</a></li>';
				newContent += '<li class="five"><a href="#" title="5 Stars" onclick="changeRating(\'prix\', \''+favorites[h].id+'\', 5)">5</a></li></ul></div></td>';
				newContent += '<td><input type="hidden" name="notePrix" id="notePrix'+favorites[h].id+'" value="0"></td>';
				
				//Notation H_Q
				newContent += '<td>Hygi�ne-Qualit� </td><td><div role="notation"><ul id="qualite'+favorites[h].id+'" class="rating nostar">';
				newContent += '<li class="one"><a href="#" title="1 Star" onclick="changeRating(\'qualite\', \''+favorites[h].id+'\', 1)">1</a></li>';
				newContent += '<li class="two"><a href="#" title="2 Stars" onclick="changeRating(\'qualite\', \''+favorites[h].id+'\', 2)">2</a></li>';
				newContent += '<li class="three"><a href="#" title="3 Stars" onclick="changeRating(\'qualite\', \''+favorites[h].id+'\', 3)">3</a></li>';
				newContent += '<li class="four"><a href="#" title="4 Stars" onclick="changeRating(\'qualite\', \''+favorites[h].id+'\', 4)">4</a></li>';
				newContent += '<li class="five"><a href="#" title="5 Stars" onclick="changeRating(\'qualite\', \''+favorites[h].id+'\', 5)">5</a></li></ul></div></td>';
				newContent += '<td><input type="hidden" name="noteQualite" id="noteQualite'+favorites[h].id+'" value="0"><td></table></p>';
				
				//Services wifi, animaux, voiturier
				newContent += '<p class="services">WiFi<label>';
				if (favorites[h].wifi == 'oui'){
					newContent += '<input type="checkbox" name="wifi" checked><span></span></label>';
				}else{ 
					newContent += '<input type="checkbox" name="wifi"><span></span></label>';
				}
				
				if (favorites[h].animaux == 'oui'){
					newContent += 'Animaux<label><input type="checkbox" name="animaux" checked><span></span></label>';
				}else{
					newContent += 'Animaux<label><input type="checkbox" name="animaux"><span></span></label>';
				}
				
				if (favorites[h].voiturier == 'oui'){
					newContent += 'Voiturier<label><input type="checkbox" name="voiturier" checked><span></span></label></p>';
				}else{
					newContent += 'Voiturier<label><input type="checkbox" name="voiturier"><span></span></label></p>';
				}
				
				//Avis
				newContent += '<fieldset><legend>Avis</legend><section><p><textarea type="tel" name="avis" required>'+favorites[h].avis+'</textarea><button type="reset">Clear</button></p><p><p></section></fieldset></p>';
				newContent += '<p><menu><button type="submit" onclick="updateFavorite(\''+favorites[h].id+'\')">Valider</button><button class="danger">Annuler</button></menu></p>';
				newContent += '</form></section>';
				
				document.getElementById(section + num).innerHTML = newContent;
				break;
			}
		}
	}
	

}


// fonction de modification des donn�es d'un favori
function updateFavorite(id){

   // ************************************************
   //******* il faut recuperer l'id du favoris id
   // var id_favorite =documnet ...........  et le passe ne paramettre de update c'est la l'erreur 
	var nom = document.forms["form"+id].nom.value;
	var adresse = document.forms["form"+id].adresse.value;
	var telephone = document.forms["form"+id].telephone.value;
	var prix = document.forms["form"+id].notePrix.value;
	var wifi = isTrue(document.forms["form"+id].wifi.checked);
	var animaux = isTrue(document.forms["form"+id].animaux.checked);
	var voiturier = isTrue(document.forms["form"+id].voiturier.checked);
	var qualite = document.forms["form"+id].noteQualite.value;
	var avis = document.forms["form"+id].avis.value;
	var longitude = document.forms["form"+id].longitude.value;
	var latitude = document.forms["form"+id].latitude.value;
	
	
	//alert(id+nom+adresse+telephone+prix+wifi+animaux+voiturier+qualite+avis+longitude+latitude);

	//Mise � jour
	DAO.updateFavorite(id,nom,adresse,telephone,prix,wifi,animaux,voiturier,qualite,avis,longitude,latitude);
	alert("Les donn�es de l'h�tel "+nom+" sont modifi�es!");

//	location.reload(); // ne pas rafrichier toute la page 
uploadFavorites() ; // reafficher les donnees apr�s update
}


// fonction de suppression d'un favori
function deleteFavorite(id){

	var rep = confirm("Voulez-vous supprimer cet h�tel?");
	if (rep == true){
		DAO.deleteFavorite(id);
		alert("L'h�tel est supprim�!");
	}
	
	//location.reload();   // ne pas reafrechir toute l'appli
	uploadFavorites() ; // reafficher les datas apr�s delete
}

// fonction qui renvoie oui si la variable est � true, non sinon
function isTrue(val){
	if(val == true)
		return "oui";
	else
		return "non";

}

// fonction de notation
function changeRating(list, id, num){
	var liste = document.getElementById(list+id);

	switch (num) {
		 case 1:
			liste.className="rating onestar";
			break;
		 case 2:
			liste.className="rating twostar";
			break;
		 case 3:
			liste.className="rating threestar";
			break;
		 case 4:
			liste.className="rating fourstar";
			break;
		 default: 
			liste.className="rating fivestar";
			break;
	}

	if (list == 'prix'){
		document.getElementById('notePrix'+id).value=num;
	}else{
		document.getElementById('noteQualite'+id).value=num;
	}
}
