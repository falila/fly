/***
 * 
 * Auteur: souleymane keita	
 *	version:2.0
 *ce fichier contient: la classe ResultRecherch ,les methodes de calculs d'itineaires,de calcul de distance entre deux etablissements ou points de la carte,
 * ainsi que celles de geolocalisation , de d'affichage de la carte.
 *
 *
**/
    // variable global 
    var gLocalSearch;
    var gMap;
    var gInfoWindow;
    var gSelectedResults = [];
    var gCurrentResults = [];
    var gSearchForm;
	var mlongitude;
	var mlatitude;
	var destLong;
	var destLat;
	var markerclic;
	// creation des markers icons . 
	
    var gYellowIcon = new google.maps.MarkerImage("img/mm_20_yellow.png",
      new google.maps.Size(12, 20),
      new google.maps.Point(0, 0),
      new google.maps.Point(6, 20));
    var gRedIcon = new google.maps.MarkerImage(
      "img/mm_20_red.png",
      new google.maps.Size(12, 20),
      new google.maps.Point(0, 0),
      new google.maps.Point(6, 20));
    var gSmallShadow = new google.maps.MarkerImage(
      "img/mm_20_shadow.png",
      new google.maps.Size(22, 20),
      new google.maps.Point(0, 0),
      new google.maps.Point(6, 20));
	 var gMaPosition = new google.maps.MarkerImage("marker_me.png",
      new google.maps.Size(22, 20),
      new google.maps.Point(0, 0),
      new google.maps.Point(6, 20));
	
	 // paramettrage de la map pour la recherche local .
	 
    function OnLoad() 
    {
		getLocation();
		var lg;
		var lat;
		if(mlongitude){
			lg=mlongitude;
			lat=mlatitude;
		}else{
			lg=2.3522219;
			lat=48.8566140;
		}
	   // Initialiasation de la carte par la vue par defaut(position de l'utilisateur ).
		gMap = new google.maps.Map(document.getElementById("map"), {
                center: new google.maps.LatLng(lat, lg),
                zoom: 13,
                mapTypeId: 'roadmap'
              });
	          //creer une infowindow a ouvrir quand on clique sur un marker 
              gInfoWindow = new google.maps.InfoWindow;
              google.maps.event.addListener(gInfoWindow, 'closeclick', function() {
                unselectMarkers();
              });

              // Initialisation de la recherche local 
              gLocalSearch = new GlocalSearch();
              gLocalSearch.setSearchCompleteCallback(null, OnLocalSearch);
	  
    } // fin onload 

    function unselectMarkers()
     {
    
      for (var i = 0; i < gCurrentResults.length; i++)
       {
        gCurrentResults[i].unselect();
       }
    } // fin unselectMarkers

    function doSearch() 
    {
    
      var query = document.getElementById("queryInput").value;
      gLocalSearch.setCenterPoint(gMap.getCenter());
      gLocalSearch.execute(query);
    } // fin dosearch

    	// appeller quand les resultats de la rechercher sont retourn�s,nous effa�ons l'ancienne
        //valeur et les resultats sont charg�s pour les news elements

    function OnLocalSearch()
     {
     
      if (!gLocalSearch.results) return;
      var searchWell = document.getElementById("searchwell");

      // effacer la map et les resultats 
      searchWell.innerHTML = "";
      for (var i = 0; i < gCurrentResults.length; i++) {
        gCurrentResults[i].marker().setMap(null);
      }
     // fermeture de infowindows
      gInfoWindow.close();

      gCurrentResults = [];
      for (var i = 0; i < gLocalSearch.results.length; i++) {
        gCurrentResults.push(new LocalResult(gLocalSearch.results[i]));
      }

      var attribution = gLocalSearch.getAttribution();
      if (attribution) {
        document.getElementById("searchwell").appendChild(attribution);
      }

      
	  // placer le premier resultat sur la carte 
      var first = gLocalSearch.results[0];
      gMap.setCenter(new google.maps.LatLng(parseFloat(first.lat),parseFloat(first.lng)));

    } //onlocationSearch

   // annuler l'envoie et l'execution de  la recherche ajax
    function CaptureForm(searchForm) {
      gLocalSearch.execute(searchForm.input.value);
      return false;
    } // fin captureform

    /******** cette class represente le type de resultat retourner par la recherche local ****/
    
    function LocalResult(result) {
      var me = this;
      me.result_ = result;
      me.resultNode_ = me.node();
      me.marker_ = me.marker();
      google.maps.event.addDomListener(me.resultNode_, 'mouseover', function() {
        //Mettez en surbrillance l'ic�ne du marqueur et le r�sultat lorsque le r�sultat est
        //mouseovered. Ne pas enlever toute surbrillance autres en ce moment.
        me.highlight(true);
      });
      google.maps.event.addDomListener(me.resultNode_, 'mouseout', function() {
        // Supprimer le surlignage � moins que ce marqueur est s�lectionn� (l'info
         //Fen�tre est ouverte).
        if (!me.selected_) me.highlight(false);
      });
      google.maps.event.addDomListener(me.resultNode_, 'click', function() {
        me.select();
        destLat=parseFloat(me.result_.lat);
  	  	destLong=parseFloat(me.result_.lng);
      });
	  
	  google.maps.event.addDomListener(me.resultNode_, 'dblclick', function() {
		
		 
       if(me.selected_)  me.addfavorite();  // dans le dom 
      });
      document.getElementById("searchwell").appendChild(me.resultNode_);
    }

	
	// fin class LocalResult
    LocalResult.prototype.node = function() {
      if (this.resultNode_) return this.resultNode_;
      return this.html();
    };

    // class favorite 
    function  Favorite (id,nom,addresse,telephone,prix,wifi,animaux,voiturier,h_q,avis,longitude,latitude){
    	
    	id=id;
    	nom=nom;
    	addresse=addresse;
    	telephone=telephone;
    	prix=prix;
    	wifi=wifi;
    	animaux=animaux;
    	voiturier=voiturier;
    	h_q=h_q;
    	avis=avis;
    	longitude=longitude;
    	latitude=latitude; 	
    	
    }
    
    Favorite.prototype.toString=function(){
    
    console.log("tostring Favorite");
    
    console.log(this.id +"-"+ this.nom +"-"+this.addresse +"-"+this.telephone+"-"+this.prix+"-"+this.wifi+"-"+
    this.animaux+"-"+this.voiturier+"-"+this.h_q+"-"+this.avis+"-"+this.longitude+"-"+this.latitude+ "--");
    }
    
    /**
     * ajoute l'etablissement h�telier selectionn� dans le favoris de l'user
     */
    function setFavorite()
    {
    	  
    	 var newFavorite=new Favorite();  	
    	 
    	 /*** verifie si 1 hotel selectioner **/
    	if(!markerclic){
    		window.confirm("Selectionner d'abord un hotel sur la carte");
    		return ;
    	}
    	//console.log(markerclic.result_.titleNoFormatting);
        //console.log(markerclic.addressLines);
        var phonenumber=markerclic.result_.phoneNumbers;
       // console.log(phonenumber[0].number);
       //	console.log(parseFloat(markerclic.result_.lat));
  	  	//console.log(parseFloat(markerclic.result_.lng));
  	  	//console.log(markerclic.result_);
  //	console.log(" addresse" +markerclic.result_.streetAddress + ","+markerclic.result_.city +" "+ markerclic.result_.country);
  	 
  	  newFavorite.id=DAO.createNewFavoriteKey();
  	  newFavorite.nom=markerclic.result_.titleNoFormatting;
  	  newFavorite.addresse=markerclic.result_.streetAddress + ", "+markerclic.result_.city +" "+ markerclic.result_.country;
  	  newFavorite.telephone=phonenumber[0].number;
  	  newFavorite.longitude=markerclic.result_.lng;
  	  newFavorite.latitude=	markerclic.result_.lat;
	  newFavorite.h_q=0;
	  newFavorite.voiturier="none";
	  newFavorite.wifi="none";
	  newFavorite.animaux="none";
	  newFavorite.prix=0;
	  newFavorite.avis="none";
  	  DAO.saveFavoriteFormMap(newFavorite);
  	  newFavorite.toString();
  	  
  	  console.log( "affichage liste favorite:")
  	  var liste=DAO.getAllFavorite();
  	 // console.log(JSON.parse(liste));
	  uploadFavorites() ;
    	}    
    
    /**
     * calcule la distance entre l'user et l'hotel indiqu� .
     */
    
    function distance(startlatitude,startlongitude,destlatitude,destlongitude) {

		var startLatRads = degreesToRadians(startlatitude);
		var startLongRads = degreesToRadians(startlongitude);
		var destLatRads = degreesToRadians(destlatitude);
		var destLongRads = degreesToRadians(destlongitude);
		var Radius = 6371; //  le rayon de la terre en km .
		var distance = Math.acos(Math.sin(startLatRads) * Math.sin(destLatRads) +
		Math.cos(startLatRads) * Math.cos(destLatRads) *
		Math.cos(startLongRads - destLongRads)) * Radius;
		return distance ;

    } // Distance
    /**
     * converti un degree en radians 
     */
	function degreesToRadians(degrees) {
	var radians = (degrees * Math.PI)/180;
	return radians;
	} // fin degreesToRadians
	// retourne le gmpa marker pour le resultat  et lui passe les icons si cela n'a pas ete fait encore 
   
    LocalResult.prototype.marker = function() {
      var me = this;
      if (me.marker_) return me.marker_;
      var marker = me.marker_ = new google.maps.Marker({
        position: new google.maps.LatLng(parseFloat(me.result_.lat),
                                         parseFloat(me.result_.lng)),
        icon: gYellowIcon,
        shadow: gSmallShadow,
        map: gMap});
      google.maps.event.addListener(marker, "click", function() {
        me.select();
      
      });
      google.maps.event.addListener(marker,"dblclick",function(){
    	 
    	// fait rien    	
      });
      return marker;
    };
     
    
	// deselectionne tous les autres marker avant d'avant d'afficher et selectr un autre marker 
    
    LocalResult.prototype.select = function() {
      unselectMarkers();
      markerclic=this;
      this.selected_ = true;
      this.highlight(true);
      destLat=parseFloat(this.result_.lat);
	  destLong=parseFloat(this.result_.lng);
		
      gInfoWindow.setContent(this.html(true));
      
      gInfoWindow.open(gMap, this.marker());
    };

    LocalResult.prototype.isSelected = function() {
      return this.selected_;
    };

    // cacher/supprimer les infos quand items n'est plus selectionner.
    LocalResult.prototype.unselect = function() {
      this.selected_ = false;
      this.highlight(false);
    };

	// selectionne le marker et le place dans une liste pour l'ajouter au favoris
   
	LocalResult.prototype.addfavorite=function(){
		
		if(this.selected_){
			
			var x =parseFloat(this.result_.lat);
			var y=parseFloat(this.result_.lng);
			destLong=y;
		    destLat=x;
			//console.log(y);
			//console.log(x);
			//console.log(this.html(true)); // return la div contenant les infos de l'hotel 
					
		}else{
		
			
		}
		 
	};
  
	// retourn le html a afficher dans la section resultat avant de ranger
    LocalResult.prototype.html = function() {
      var me = this;
      var container = document.createElement("div");
      container.className = "unselected";
      container.appendChild(me.result_.html.cloneNode(true));
      return container;
    };

    LocalResult.prototype.highlight = function(highlight) {
      this.marker().setOptions({icon: highlight ? gRedIcon : gYellowIcon});
      this.node().className = "unselected" + (highlight ? " red" : "");
    };

    GSearch.setOnLoadCallback(OnLoad);
    /******
     *  section gestion de la localisation
     */

	// verication de l'activation de la geolocation 
			function getLocation()
		  {
			  if (navigator.geolocation)
				{
				navigator.geolocation.getCurrentPosition(maPosition,showError);
				}
			  else{infogeo.innerHTML="Geolocation is not supported./ non support� ";}
		  
		 } // fin getlocation
	// recuperation de la position actuel de l'utilisateur 
		function maPosition(position)
		{
			mlatitude=position.coords.latitude;
			mlongitude=position.coords.longitude;
			// Initialiasation de la carte par la vue par defaut(position de l'utilisateur ).
			// streetviewcontrol ,
			var options = {
					zoom: 14,
					center: new google.maps.LatLng(mlatitude, mlongitude),
					streetViewControl: true,
					mapTypeId: google.maps.MapTypeId.ROADMAP};
			
			gMap = new google.maps.Map(document.getElementById("map"),options); 
			var coords=new google.maps.LatLng(mlatitude,mlongitude);
			addMarker(gMap, coords, "You"," Your Position");
			doSearch();
				
		} // fin maPosition
		
		// ajout de la position de l'utilisateur sur la map 
		function addMarker(map, latlong, title, content) {
			var markerOptions = {
				position: latlong,
				map: map,
				title: title,
				clickable: true
			};
			var marker = new google.maps.Marker(markerOptions);

			var infoWindowOptions = {
				content: content,
				position: latlong,
			};

			var infoWindow = new google.maps.InfoWindow(infoWindowOptions);

			google.maps.event.addListener(marker, 'click', function() {
				infoWindow.open(map);
			});
		}// fin addMarker
		
	// gestion des erreurs 
		function showError(error)
	  {
	  switch(error.code) 
		{
		case error.PERMISSION_DENIED:
		  infogeo.innerHTML="User denied the request for Geolocation."
		  break;
		case error.POSITION_UNAVAILABLE:
		  infogeo.innerHTML="Location information is unavailable/info de geolocalisation non dispo"
		  break;
		case error.TIMEOUT:
		  infogeo.innerHTML="The request to get user location timed out/temps ecoul�."
		  break;
		case error.UNKNOWN_ERROR:
		  infogeo.innerHTML="An unknown error occurred/une erreur c'est produite."
		  break;
		}
	  } // fin showError
	  
    // calcul de l'itineire 
		
		function calculItineaire(){
			
			if(!markerclic){
				window.confirm("Selectionnez un hotel sur la carte");
	    		return ;
			}
			// on centre la map a paris 
			var centreCarte = new google.maps.LatLng(47.389982, 0.688877);
			var optionsCarte = {
				zoom: 8,
				center: new google.maps.LatLng(mlatitude, mlongitude),
				mapTypeId: google.maps.MapTypeId.ROADMAP
			}
			var itineraireCarte = new google.maps.Map(document.getElementById("map"), optionsCarte);
			/**
			 * Moteur de rendu
			 */
			var optionsItineraireAffichage = {
				map: itineraireCarte,
				panel: document.getElementById("itineraireTexte")
			}
			var itineraireAffichage = new google.maps.DirectionsRenderer(optionsItineraireAffichage);
			/**
			 * Service itin�raire
			 */
			console.log("appel service directin");
			var itineraireService = new google.maps.DirectionsService();
			/**
			 * Objet litt�ral
			 */
			 var itineraireRequete = {
				origin: new google.maps.LatLng(mlatitude, mlongitude),
				destination: new google.maps.LatLng(destLat, destLong),
				travelMode: google.maps.TravelMode.DRIVING
			}
			/**
			 * Envoie la requ�te vers les serveurs Google (Asynchrone)
			 */
			itineraireService.route(itineraireRequete, function(itineraireResultat, itineraireCodeStatut) {
				/**
				 * Si le r�sultat est valide on demande au moteur de rendu
				 * d'utiliser ce r�sultat pour mettre � jour l'affichage
				 * de l'itin�raire ( carte + roadbook)
				 */
				if (itineraireCodeStatut === google.maps.DirectionsStatus.OK) {
					 itineraireAffichage.setDirections(itineraireResultat);
				/**
				 * Sinon on affiche le code erreur
				 */
				}else{
					alert('Erreur : ' + itineraireCodeStatut);
				}
			});
			 $("itineraireTexte").hide();
		} // fin function
		
		
				 
	    	      
		



